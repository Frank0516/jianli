import { DOCUMENT } from '@angular/common';
import { Inject, Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { Subject } from 'rxjs';

export enum ModeAction {
    LANG = 'lang',
    MODE = 'mode',
}

export interface SubscribeAction<T = any, P = any> {
    type: T;
    payload?: P;
}

@Injectable({
    providedIn: 'root',
})
export class TranslationService {
    private rd2: Renderer2;

    private currentLang = 'zh';
    private currentTheme = '';
    private translation$ = new Subject<SubscribeAction<ModeAction>>();
    private switch$ = new Subject<SubscribeAction<ModeAction>>();

    constructor(
        @Inject(DOCUMENT) private doc: Document,
        rd2f: RendererFactory2
    ) {
        this.rd2 = rd2f.createRenderer(null, null);
        this.subscribeAction();
    }

    dispatch({ type, payload }: SubscribeAction) {
        this.translation$.next({ type, payload });
    }

    switchChange() {
        return this.switch$.asObservable();
    }

    private subscribeAction() {
        this.translation$.asObservable().subscribe((res) => {
            this.action(res);
        });
    }

    private action(action: SubscribeAction) {
        const htmlDom = this.doc.getRootNode().childNodes[1];
        switch (action.type) {
            case ModeAction.LANG:
                this.currentLang = action.payload;
                localStorage.setItem('lang', this.currentLang);
                this.rd2.setAttribute(htmlDom, 'lang', this.currentLang);
                this.switch$.next({
                    type: action.type,
                    payload: this.currentLang,
                });
                break;
            case ModeAction.MODE:
                this.currentTheme = action.payload;
                localStorage.setItem('theme', this.currentTheme);
                this.rd2.setAttribute(htmlDom, 'data-theme', this.currentTheme);
                this.switch$.next({
                    type: action.type,
                    payload: this.currentTheme,
                });
                break;
        }
    }
}
