import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimelineComponent } from './timeline.component';
import { TimelineItemComponent } from './timeline-item.component';
import { TitleModule } from '../title/title.module';

@NgModule({
    declarations: [TimelineComponent, TimelineItemComponent],
    imports: [CommonModule, TitleModule],
    exports: [TimelineComponent, TimelineItemComponent],
})
export class TimelineModule {}
