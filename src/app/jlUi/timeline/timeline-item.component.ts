import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'jl-timeline-item',
    template: `
        <article class="timeline-item">
            <h6 class="h6 sub-title">{{ title }}</h6>
            <span class="time my1 block">{{ time }}</span>
            <p class="mb1">{{ desc }}</p>
        </article>
    `,
    styleUrls: ['./timeline-item.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TimelineItemComponent {
    @Input() title: string = '';
    @Input() time: string = '';
    @Input() desc: string = '';

    constructor() {}
}
