import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

interface TimelineOption {
    title: string;
    desc: string;
    time: string;
}

@Component({
    selector: 'jl-timeline',
    template: `
        <div>
            <jl-title [type]="titleType" [isEmbellishUnderline]="false">
                <ng-content></ng-content>
            </jl-title>
            <div class="mt20 px10">
                <ng-container *ngFor="let item of timelineOption">
                    <jl-timeline-item
                        [title]="item.title"
                        [desc]="item.desc"
                        [time]="item.time"
                    ></jl-timeline-item>
                </ng-container>
            </div>
        </div>
    `,
    styles: [
        `
            .timeline-box
                jl-timeline-item:last-child
                ::ng-deep
                article::before {
                width: 0;
            }
        `,
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TimelineComponent {
    @Input() timelineOption: TimelineOption[] = [];
    @Input() title: string = '';
    @Input() titleType: 'title' | 'subTitle' = 'subTitle';

    constructor() {}
}
