import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'jl-footer',
    template: `
        <footer class="text-center py30">
            All Copyright &copy; 2022 ~ {{ year }}.&nbsp; Power By Frank Gong.
        </footer>
    `,
    styles: [],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterComponent implements OnInit {
    year = new Date().getFullYear();

    constructor() {}

    ngOnInit(): void {}
}
