import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgressComponent } from './progress.component';
import { TitleModule } from '../title/title.module';

@NgModule({
    declarations: [ProgressComponent],
    imports: [CommonModule, TitleModule],
    exports: [ProgressComponent],
})
export class ProgressModule {}
