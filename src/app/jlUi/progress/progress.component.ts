import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'jl-progress',
    template: `
        <div class="progress-wrapper">
            <div
                class="progress-bar px10 flex flex-center f-jc-sb"
                [ngStyle]="{ width: _progress + '%' }"
            >
                <span>{{ name }}</span>
                <span>{{ _progress }}%</span>
            </div>
        </div>
    `,
    styles: [
        `
            .progress-wrapper {
                width: 100%;
                border-radius: 2rem;
                overflow: hidden;
                background-color: var(--light);
                height: 28px;
                color: var(--white);
                font-size: 1.1rem;
            }

            .progress-bar {
                height: 100%;
                border-radius: 2rem;
                background: linear-gradient(
                    to right,
                    var(--secondary) 0%,
                    var(--primary) 100%
                );
                box-shadow: 0 2px 4px rgba(240, 0, 86, 0.2),
                    0 8px 16px rgba(240, 0, 86, 0.3);
                transition: all 0.5s ease-in-out;
            }
        `,
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProgressComponent {
    _progress = 0;

    @Input() name: string = '';
    @Input() set progress(rate: number) {
        if (typeof rate === 'number' && !isNaN(rate)) {
            const real = Math.max(Math.min(100, rate), 0);
            this._progress = real;
        }
    }

    constructor() {}
}
