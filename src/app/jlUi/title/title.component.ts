import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'jl-title',
    template: ` <ng-content></ng-content> `,
    changeDetection: ChangeDetectionStrategy.OnPush,
    host: {
        class: 'block',
        '[class.h3]': `type === 'title'`,
        '[class.title]': `type === 'title'`,
        '[class.mb4]': `type === 'title'`,
        '[class.h4]': `type === 'subTitle'`,
        '[class.sub-title]': `type === 'subTitle'`,
        '[class.embellish-underline]': `isEmbellishUnderline`,
    },
})
export class TitleComponent {
    @Input() type: 'title' | 'subTitle' = 'title';
    @Input() isEmbellishUnderline: boolean = true;

    constructor() {}
}
