import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TitleModule, FooterModule, TimelineModule } from './jlUi';
import { DoingModule, FixedWidgetsModule, SkillsModule } from './components';
import LocaleMSG from './i18n/message';
import { TranslationModule } from './pipes';

(window as any).FG_MESSAGE = LocaleMSG;

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        TitleModule,
        FooterModule,
        SkillsModule,
        TimelineModule,
        TranslationModule,
        FixedWidgetsModule,
        DoingModule,
    ],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
