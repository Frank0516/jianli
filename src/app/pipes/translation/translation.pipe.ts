import { Pipe, PipeTransform } from '@angular/core';
import { filter, firstValueFrom } from 'rxjs';
import { ModeAction, TranslationService } from 'src/app/services';

@Pipe({
    name: 'translation',
    pure: false,
})
export class TranslationPipe implements PipeTransform {
    private realValue = '';

    constructor(private switchService: TranslationService) {}

    transform(value: string) {
        this.getLang(value);
        return this.realValue || this.defaultValue(value);
    }

    async getLang(value: string) {
        const res = await firstValueFrom(
            this.switchService
                .switchChange()
                .pipe(filter((f) => f?.type === ModeAction.LANG))
        );
        let text = value;
        if (res?.type === ModeAction.LANG) {
            text = (window as any)?.FG_MESSAGE[res.payload][value] || value;
        }
        this.realValue = text;
    }

    defaultValue(value: string) {
        const lang = localStorage.getItem('lang') || 'zh';
        return (window as any)?.FG_MESSAGE[lang][value] || value;
    }
}
