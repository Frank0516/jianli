import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslationPipe } from './translation.pipe';
import { TranslationService } from 'src/app/services';



@NgModule({
  declarations: [
    TranslationPipe
  ],
  imports: [
    CommonModule
  ],
  providers: [TranslationService],
  exports: [TranslationPipe]
})
export class TranslationModule { }
