export default {
    zh: {
        name: '龚元文',
        position: '前端开发工程师',
        birth: '06.28',
        location: '杭州',
        mail: 'yu***16@a***n.com',
        phoneNo: '+86 15******554',
        aboutMeTitle: '关于我',
        resume: '简历',
        skillsTitle: '技能',
        doing: '工作事项',
        doingList: [
            {
                title: '网页设计',
                desc: '了解最新的设计趋势，有一定的审美标准，需求的实现都是使用figma或即时设计app来完成设计',
                icon: 'design',
            },
            {
                title: '网页开发',
                desc: '熟悉开发流程，熟悉前端框架(angular、react)，了解vue，具有一定的后台开发能力(node)',
                icon: 'web',
            },
            {
                title: '小程序开发',
                desc: '了解各大平台的小程序开发流程，目前已经开发过几款小程序，都能够稳定运行',
                icon: 'app',
            },
            {
                title: '摄影',
                desc: '业余爱好摄影，掌握基本的摄影技巧，擅长拍风景和人物，照片风格柔美、亮丽、意境',
                icon: 'photography',
            },
        ],
        eductionTitle: '教育经历',
        eduction: [
            {
                title: '上海交通大学',
                desc: '本科、计算机科学与技术，主要学习课程c++、java、计算机原理、计算机安全等',
                time: '2018.3 - 2020.6',
            },
            {
                title: '东华大学',
                desc: '专科、数字媒体艺术专业，主要学习艺术设计、摄影',
                time: '2015.3 - 2017.6',
            },
            {
                title: '南鄂高中',
                desc: '全日制、高中、理科，全面素质教育和基础知识学习',
                time: '2008.9 - 2011.6',
            },
        ],
        experienceTitle: '工作经验',
        experience: [
            {
                title: '杭州博彦信息科技',
                subTitle: '前端开发',
                desc: '公司属于外包性质，驻场阿里云谷园区，主要工作内容：阿里云官网页面、内部系统、内部平台等的开发和维护，使用技术：angular、react',
                time: '2020.7 - 至今',
            },
            {
                title: '天津联合通商科技',
                subTitle: '前端开发',
                desc: '该公司是台企，一家做销售预测的公司，工作内容主要是用angular来开发和维护预测系统页面',
                time: '2018.12 - 2020.07',
            },
            {
                title: '上海朝营信息科技',
                subTitle: '前端开发',
                desc: '外包公司，驻场绿盟，绿盟是做安防技术的公司，主要职责是开发大屏动态感知内动的页面，使用的技术angular、echart等',
                time: '2018.05 - 2018.12',
            },
        ],
        skillsOne: [
            {
                label: 'Javascript',
                value: 83,
            },
            {
                label: 'Typescript',
                value: 80,
            },
            {
                label: 'Css',
                value: 81,
            },
            {
                label: 'Html',
                value: 85,
            },
        ],
        skillsTwo: [
            {
                label: 'Angular',
                value: 80,
            },
            {
                label: 'React',
                value: 70,
            },
            {
                label: 'Node',
                value: 40,
            },
            {
                label: 'Nest',
                value: 30,
            },
        ],
        introDesc:
            '一个湖北人在杭州，本人个性开朗带一点活泼，为人随和，爱好比较广泛，比如：诗词歌赋、琴棋书画，旅行、和平等，还有就是对古风比较有兴趣，经常和同志一起穿汉服出门玩耍。独处时，一般是学习或者打王者，学习的内容都是工作和爱好相关；共处时，可以逗笑别人和化解尴尬，能够和同伴和谐共处。',
        skillDesc:
            '目前就职于博彦，驻场阿里巴巴办公，从事前端开发工作，已经有5年开发经验，熟练掌握angular框架的开发技巧，熟悉react的开发流程，它们的原理略有涉及和了解，日常开发内容主要是组件编写、维护代码、提供需求解决方案、方案文档输出、参与前后端团队业务讨论、指导同事前端相关知识。有一套自己的开发流程规范，提高效率和降低维护成本。',
    },
    en: {
        name: 'Frank',
        position: 'Front-End Dev.',
        birth: 'June 28th',
        location: 'Hangzhou China',
        mail: 'yu***16@a***n.com',
        phoneNo: '+86 15******554',
        aboutMeTitle: 'About Me',
        resume: 'Resume',
        skillsTitle: 'Skills',
        doing: 'Can Doing',
        doingList: [
            {
                title: 'Web Design',
                desc: 'Know the latest design trends, have certain aesthetic standards, requirements are realized using figma or instant design app to complete the design.',
                icon: 'design',
            },
            {
                title: 'Web Development',
                desc: 'Familiar with development process, front-end framework (angular, react), know vue, have background development ability (node).',
                icon: 'web',
            },
            {
                title: 'Mobile Mini Apps',
                desc: 'Understand the small program development process of various platforms, so far has developed several small programs, can run stably.',
                icon: 'app',
            },
            {
                title: 'Photography',
                desc: 'Hobby photography, master the basic photography skills, good at taking pictures of scenery and figures, the style of photos soft, beautiful, artistic conception.',
                icon: 'photography',
            },
        ],
        eductionTitle: 'Eduction',
        eduction: [
            {
                title: 'SJTU',
                desc: 'Bachelor degree, computer science and technology, main courses c++, java, computer principles, computer security, etc.',
                time: '2018.3 - 2020.6',
            },
            {
                title: 'DHU',
                desc: 'Junior college, digital media art major, mainly learning art design, photography.',
                time: '2015.3 - 2017.6',
            },
            {
                title: 'NanE High school',
                desc: 'Full-time, high school, science, comprehensive quality education and basic knowledge study.',
                time: '2008.9 - 2011.6',
            },
        ],
        experienceTitle: 'Experience',
        experience: [
            {
                title: 'Hangzhou Boyan Information Technology',
                desc: 'The company is an outsourcing company located in Alibaba YunGu Park. Main work contents: development and maintenance of Ali Cloud official website page, internal system and internal platform, etc., and application technologies: angular and react.',
                time: '2020.7 - Present',
            },
            {
                title: 'eBizprise Technology(TianJin) Limited',
                desc: 'The company is from Taiwan of China, sales forecasting company that uses angular to develop and maintain forecasting system pages.',
                time: '2018.12 - 2020.07',
            },
            {
                title: 'Shanghai ChaoYing Information Technology',
                desc: 'Green League is a security technology company. Its main responsibility is to develop large screen dynamic sensing internal pages, using angular, echart and other technologies.',
                time: '2018.05 - 2018.12',
            },
        ],
        skillsOne: [
            {
                label: 'Javascript',
                value: 83,
            },
            {
                label: 'Typescript',
                value: 80,
            },
            {
                label: 'Css',
                value: 81,
            },
            {
                label: 'Html',
                value: 85,
            },
        ],
        skillsTwo: [
            {
                label: 'Angular',
                value: 80,
            },
            {
                label: 'React',
                value: 70,
            },
            {
                label: 'Node',
                value: 40,
            },
            {
                label: 'Nest',
                value: 30,
            },
        ],
        introDesc:
            'A person from Hubei Province lives in Hangzhou. I have a cheerful personality and a little lively. I am easy-going and have a wide range of hobbies, such as poetry and poetry, Qin, chess, calligraphy and painting, travel, and equality. Alone time, usually study or play game that is Honor of Kings, study content is related to work and hobbies; When you are together, can make people laugh and resolve embarrassment, can get along well with peers.',
        skillDesc:
            "I'm currently working in Boyan, working in Alibaba, engaged in front-end development work. I have 5 years of development experience. I am familiar with the development skills of angular framework, familiar with the development process of react, and have a slight understanding of their principles. The daily development contents mainly include component writing, code maintenance, requirement solutions, solution document output, participation in front - and back-end team business discussions, and guidance of colleagues on front-end related knowledge. A set of own development process specifications, improve efficiency and reduce maintenance costs.",
    },
};
