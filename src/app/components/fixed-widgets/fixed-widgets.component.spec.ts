import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FixedWidgetsComponent } from './fixed-widgets.component';

describe('FixedWidgetsComponent', () => {
  let component: FixedWidgetsComponent;
  let fixture: ComponentFixture<FixedWidgetsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FixedWidgetsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FixedWidgetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
