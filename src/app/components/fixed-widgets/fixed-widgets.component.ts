import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
} from '@angular/core';

@Component({
    selector: 'jl-fixed-widgets',
    templateUrl: './fixed-widgets.component.html',
    styleUrls: ['./fixed-widgets.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FixedWidgetsComponent implements OnInit {
    @Output() readonly langChange = new EventEmitter();

    @Input() currentLang = localStorage.getItem('lang') || 'zh';

    constructor() {}

    ngOnInit(): void {}

    handleChangeLang(ev: Event) {
        ev.stopPropagation();
        ev.preventDefault();
        this.langChange.emit();
    }
}
