import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FixedWidgetsComponent } from './fixed-widgets.component';

@NgModule({
    declarations: [FixedWidgetsComponent],
    imports: [CommonModule],
    exports: [FixedWidgetsComponent],
})
export class FixedWidgetsModule {}
