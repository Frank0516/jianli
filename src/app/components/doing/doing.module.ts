import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DoingComponent } from './doing.component';

@NgModule({
    declarations: [DoingComponent],
    imports: [CommonModule],
    exports: [DoingComponent],
})
export class DoingModule {}
