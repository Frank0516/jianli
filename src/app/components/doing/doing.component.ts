import {
    ChangeDetectionStrategy,
    Component,
    Input,
    OnInit,
} from '@angular/core';

@Component({
    selector: 'jl-doing',
    template: `
        <div class="flex doing-box p3">
            <div class="doing-icon">
                <ng-content></ng-content>
            </div>
            <div class="doing-content ml2">
                <h5 class="h5 title">{{ title }}</h5>
                <p class="desc mt05">
                    {{ desc }}
                </p>
            </div>
        </div>
    `,
    styles: [
        `
            .doing-box {
                background-color: #fff;
                border-radius: 0.937rem;
                box-shadow: 0px 4px 8px rgba(134, 151, 168, 0.1);
            }

            .doing-box .doing-content .desc {
                color: var(--brightness2);
            }
        `,
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DoingComponent implements OnInit {
    @Input() title: string = '';
    @Input() desc: string = '';

    constructor() {}

    ngOnInit(): void {}
}
