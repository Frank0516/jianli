import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SkillsComponent } from './skills.component';
import { ProgressModule } from 'src/app/jlUi';

@NgModule({
    declarations: [SkillsComponent],
    imports: [CommonModule, ProgressModule],
    exports: [SkillsComponent],
})
export class SkillsModule {}
