import {
    ChangeDetectionStrategy,
    Component,
    Input,
    OnInit,
} from '@angular/core';

interface Skill {
    label: string;
    value: number;
}

@Component({
    selector: 'jl-skills',
    template: `
        <div class="skill-box">
            <div class="progress" *ngFor="let item of skills">
                <jl-progress
                    [name]="item.label"
                    [progress]="item.value"
                ></jl-progress>
            </div>
        </div>
    `,
    styles: [
        `
            .skill-box {
                padding: 20px;
                background-color: var(--white);
                box-shadow: 0px 4px 8px rgba(134, 151, 168, 0.1);
                border-radius: 1rem;
            }
            .skill-box .progress:not(:last-child) {
                margin-bottom: 20px;
            }
        `,
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SkillsComponent implements OnInit {
    @Input() skills: Skill[] = [];

    constructor() {}

    ngOnInit(): void {}
}
