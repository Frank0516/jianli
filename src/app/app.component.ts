import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ModeAction, TranslationService } from './services';
import { filter } from 'rxjs';

interface DoingListOption {
    title: string;
    desc: string;
    icon: string;
}

@Component({
    selector: 'jl-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less'],
})
export class AppComponent implements OnInit {
    eduction = [];
    experience = [];
    skillsOne = [];
    skillsTwo = [];

    doingList: DoingListOption[] = [];

    currentLang = 'zh';

    constructor(
        private switchService: TranslationService,
        private cdr: ChangeDetectorRef
    ) {
        console.log(`
        159 0179 6554
       `);
    }
    ngOnInit(): void {
        this.switchService
            .switchChange()
            .pipe(filter((f) => f.type === ModeAction.LANG))
            .subscribe((res) => {
                if (res?.type === ModeAction.LANG) {
                    this.currentLang = res.payload;
                    this.eduction = this.getText(res.payload, 'eduction');
                    this.experience = this.getText(res.payload, 'experience');
                    this.skillsOne = this.getText(res.payload, 'skillsOne');
                    this.skillsTwo = this.getText(res.payload, 'skillsTwo');
                    this.doingList = this.getText(res.payload, 'doingList');
                }
                this.cdr.markForCheck();
            });
        const lang = localStorage.getItem('lang');
        this.switchService.dispatch({
            type: ModeAction.LANG,
            payload: lang || 'zh',
        });
    }

    handleChangeLang() {
        this.switchService.dispatch({
            type: ModeAction.LANG,
            payload: this.currentLang === 'zh' ? 'en' : 'zh',
        });
    }

    private getText(lang: string, key: string) {
        return (window as any)?.FG_MESSAGE[lang][key];
    }
}
